/*

libipfix - a library which provides an implementation of the IPFIX protocol
           for flexible flow data support. IPFIX is the successor of NetFlow v9
           (see http://www.ietf.org/dyn/wg/charter/ipfix-charter.html and
            RFC5101 and RFC5102 for details)

Copyright (c) 2005-2011, Fraunhofer FOKUS
All rights reserved.

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, see <http://www.gnu.org/licenses/>.
 */
/*
**     collector.c - example ipfix collector
**
**     Copyright Fraunhofer FOKUS
**
**     $Date: 2005/08/16 12:57:57 $
**
**     $Revision: 1.4 $
**
**     todo: revise loggin
*/
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <signal.h>
#include <libgen.h>
#include <limits.h>
#include <stdarg.h>

#include "ipfix.h"
#include "ipfix_col.h"
#include "ipfix_def_fokus.h"
#include "ipfix_fields_fokus.h"
#include "misc.h"


static char       progname[30];
static int        verbose_level = 0;


extern ipfixs_node_t  *udp_sources;
int parse_body(char *buf, ssize_t len)
{
    ipfix_input_t            input;
    input.type = IPFIX_INPUT_RAW;
    //input.u.ipcon.addr = (struct sockaddr *)&caddr;
    //input.u.ipcon.addrlen = caddrlen;
    (void) ipfix_parse_msg(&input, &udp_sources, (uint8_t *)buf, len);
    return 0;
}


char ipfix_msg1[] =
{
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x45, 0x00,
    0x00, 0xae, 0xf0, 0x0f, 0x40, 0x00, 0x40, 0x11, 0x4c, 0x2d, 0x7f, 0x00, 0x00, 0x01, 0x7f, 0x00,
    0x00, 0x01, 0xc5, 0xec, 0x12, 0x83, 0x00, 0x9a, 0xfe, 0xad, 0x00, 0x0a, 0x00, 0x92, 0x57, 0x52,
    0xe5, 0x9d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x39, 0x00, 0x02, 0x00, 0x20, 0x01, 0x01,
    0x00, 0x06, 0x00, 0x1b, 0x00, 0x10, 0x00, 0x1c, 0x00, 0x10, 0x00, 0x07, 0x00, 0x02, 0x00, 0x0b,
    0x00, 0x02, 0x00, 0x04, 0x00, 0x01, 0x00, 0x02, 0x00, 0x04, 0x00, 0x02, 0x00, 0x20, 0x01, 0x00,
    0x00, 0x06, 0x00, 0x08, 0x00, 0x04, 0x00, 0x0c, 0x00, 0x04, 0x00, 0x07, 0x00, 0x02, 0x00, 0x0b,
    0x00, 0x02, 0x00, 0x04, 0x00, 0x01, 0x00, 0x02, 0x00, 0x04, 0x01, 0x00, 0x00, 0x15, 0xac, 0x1b,
    0x01, 0x1b, 0xac, 0x1c, 0x01, 0x1c, 0x0b, 0x0b, 0x63, 0x63, 0x06, 0x00, 0x00, 0x04, 0xd2, 0x01,
    0x01, 0x00, 0x2d, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
    0x18, 0x19, 0x1a, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d,
    0x0e, 0x0f, 0x10, 0x0b, 0x0b, 0x63, 0x63, 0x11, 0x00, 0x00, 0x04, 0xd2
};

void exit_func(int signo)
{
    if (verbose_level && signo)
        fprintf(stderr, "\n[%s] got signo %d, bye.\n\n", progname, signo);

    ipfix_col_cleanup();
    ipfix_cleanup();
    exit(1);
}

/*------ main ------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    char          datadir[] = ".";
    /** init loggin
    */
    mlog_set_vlevel(verbose_level);

    /** init ipfix lib
     */
    if (ipfix_init() < 0)
    {
        fprintf(stderr, "ipfix_init() failed: %s\n", strerror(errno));
        exit(1);
    }

    /** signal handler
      */
    signal(SIGKILL, exit_func);
    signal(SIGTERM, exit_func);
    signal(SIGINT,  exit_func);
    /** activate file export
     */
    (void) ipfix_col_init_fileexport(datadir);
    /** event loop
     */
    parse_body(ipfix_msg1 + 42, sizeof(ipfix_msg1) - 42);
    exit(1);
}

