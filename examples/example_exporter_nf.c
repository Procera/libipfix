/*
**     exporter.c - example exporter
**
**     Copyright Fraunhofer FOKUS
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <ipfix.h>
#include <mlog.h>

#include "ipfix_fields_nf.h"

typedef struct field{
    char descr[128];
    int length;
    int id;
} FIELD ;

typedef struct template_s{
    int testcaseId;
    FIELD fields[100];
    int fieldsCnt;
} TEMPLATE;


int enterpriseNumber = 0x1234;

#define DNS_IPV4 1
#define DNS_IPV6 2

TEMPLATE templates[] = {
    {DNS_IPV4, {{"QUERY", 65535, 2},  
                {"NAME", 65535, 3}, 
                {"HOST", 65535, 4},
                {"HOST_IPV4_ADDR", 4, 5}, 
                {"HOST_IPV6_ADDR", 16, 6}, 
                {"REVERSE_IPV4_ADDR", 4, 7}, 
                {"REVERSE_IPV6_ADDR", 16, 8}, 
                {"QUERY_TYPE", 9, 9}, 
                {"TRANSACTION_ID", 2, 10}, 
                {"MESSAGE_TYPE", 6, 11}, 
                {"HOST_TYPE", 9, 12}, 
                {"REPLY_CODE", 1, 13}, 
                {"RESPONSE_TIME", 4, 14}}, 13}
};
typedef struct input_s {
    int testcaseId;
    char fields[32][128];
} INPUT;
INPUT inputs[] = {
    {DNS_IPV4, 
     {"www.123.com: type A, class INET", 
      "www.123.com",
      "123",
      {1,2,3,4},
      {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
      {0},
      {0},
      "A",
      {1,2,3,4},
      "QUERY",
      "RR_type",
      0,
      {0,0,0,1}
     }}
};

nf_testInput (ipfix_t *ipfixh) { 
    /** get template
     */
    ipfix_template_t *ipfixt = NULL;
    int i=0;
    for (i=0; i<sizeof(inputs)/sizeof(INPUT); i++) {
        int j=0;
        for(j=0; j<sizeof(templates)/sizeof(TEMPLATE); j++) {
            if (templates[j].testcaseId == inputs[i].testcaseId) break;
        }
        if ( ipfix_new_data_template( ipfixh, &ipfixt, 
                                      templates[j].fieldsCnt ) <0 ) {
            fprintf( stderr, "ipfix_new_template() failed: %s\n", 
                     strerror(errno) );
            exit(1);
        }
        int index=0;
        for (index=0; index<templates[j].fieldsCnt; index++) {
            fprintf (stderr, "id:%d, length=%d\n", 
                     templates[j].fields[index].id, 
                     templates[j].fields[index].length);

            if (ipfix_add_field( ipfixh, ipfixt, 50500, 
                                 templates[j].fields[index].id, 
                                 templates[j].fields[index].length) <0 ) {
                fprintf( stderr, "ipfix_new_template() failed: %s\n", 
                         strerror(errno) );
                exit(1);
            }
        }
        if ( ipfix_export_nfInput ( ipfixh, ipfixt, &inputs[i]) <0) {
            fprintf( stderr, "ipfix_export() failed: %s\n", 
                     strerror(errno) );
            exit(1);
        }

        if ( ipfix_export_flush( ipfixh ) <0 ) {
            fprintf( stderr, "ipfix_export_flush() failed: %s\n", 
                     strerror(errno) );
            exit(1);
        }

        printf( "done.\n" );
        sleep(1);
    }
}

int main ( int argc, char **argv )
{
    char      *optstr="hc:p:vstu";
    int       opt;
    char      chost[256];
    int       protocol = IPFIX_PROTO_TCP;
    int       j;
    uint32_t  bytes    = 1234;
    char      buf[31]  = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                           11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                           21, 22, 23, 24, 25, 26, 27, 28, 29, 30 };

    ipfix_t           *ipfixh  = NULL;
    ipfix_template_t  *ipfixt  = NULL;
    int               domainId = 12345;
    int               port     = IPFIX_PORTNO;
    int               verbose_level = 0;

    /* set default host */
    strcpy(chost, "localhost");

    /** process command line args
     */
    while( ( opt = getopt( argc, argv, optstr ) ) != EOF )
    {
	switch( opt )
	{
	  case 'p':
	    if ((port=atoi(optarg)) <0) {
		fprintf( stderr, "Invalid -p argument!\n" );
		exit(1);
	    }
            break;

	  case 'c':
            strcpy(chost, optarg);
	    break;

          case 's':
              protocol = IPFIX_PROTO_SCTP;
              break;

          case 't':
              protocol = IPFIX_PROTO_TCP;
              break;

          case 'u':
              protocol = IPFIX_PROTO_UDP;
              break;

          case 'v':
              verbose_level ++;
              break;

	  case 'h':
	  default:
              fprintf( stderr, "usage: %s [-hstuv] [-c collector] [-p portno]\n" 
                       "  -h               this help\n"
                       "  -c <collector>   collector address\n"
                       "  -p <portno>      collector port number (default=%d)\n"
                       "  -s               send data via SCTP\n"
                       "  -t               send data via TCP (default)\n"
                       "  -u               send data via UDP\n"
                       "  -v               increase verbose level\n\n",
                       argv[0], IPFIX_PORTNO  );
              exit(1);
	}
    }

    /** init loggin
     */
    mlog_set_vlevel( verbose_level );

    /** init lib 
     */
    if ( ipfix_init() <0) {
        fprintf( stderr, "cannot init ipfix module: %s\n", strerror(errno) );
        exit(1);
    }
    if ( ipfix_add_vendor_information_elements( ipfix_ft_nf) <0 ) {
        fprintf( stderr, "ipfix_add_vendor_ie() failed: %s\n",
                 strerror(errno) );
        exit(1);
    }
    //ipfix_dump_ftinfo();

    /** open ipfix exporter
     */
    if ( ipfix_open( &ipfixh, domainId, IPFIX_VERSION ) <0 ) {
        fprintf( stderr, "ipfix_open() failed: %s\n", strerror(errno) );
        exit(1);
    }

    /** set collector to use
     */
    if ( ipfix_add_collector( ipfixh, chost, port, protocol ) <0 ) {
        fprintf( stderr, "ipfix_add_collector(%s,%d) failed: %s\n", 
                 chost, port, strerror(errno));
        exit(1);
    }

    nf_testInput (ipfixh);
#if 0
    /** get template
     */
    if ( ipfix_new_data_template( ipfixh, &ipfixt, 2 ) <0 ) {
        fprintf( stderr, "ipfix_new_template() failed: %s\n", 
                 strerror(errno) );
        exit(1);
    }
    if ( (ipfix_add_field( ipfixh, ipfixt, 
                           0, IPFIX_FT_SOURCEIPV4ADDRESS, 4 ) <0 ) 
         || (ipfix_add_field( ipfixh, ipfixt, 
                              0, IPFIX_FT_PACKETDELTACOUNT, 4 ) <0 ) ) {
        fprintf( stderr, "ipfix_new_template() failed: %s\n", 
                 strerror(errno) );
        exit(1);
    }

    /** export some data
     */
    for( j=0; j<10; j++ ) {

        printf( "[%d] export some data ... ", j );
        fflush( stdout) ;

        if ( ipfix_export( ipfixh, ipfixt, buf, &bytes ) <0 ) {
            fprintf( stderr, "ipfix_export() failed: %s\n", 
                     strerror(errno) );
            exit(1);
        }

        if ( ipfix_export_flush( ipfixh ) <0 ) {
            fprintf( stderr, "ipfix_export_flush() failed: %s\n", 
                     strerror(errno) );
            exit(1);
        }

        printf( "done.\n" );
        bytes++;
        sleep(1);
    }
#endif

    printf( "data exported.\n" );

    /** clean up
     */
    ipfix_delete_template( ipfixh, ipfixt );
    ipfix_close( ipfixh );
    ipfix_cleanup();
    exit(0);
}
