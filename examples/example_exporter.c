/*
**     exporter.c - example exporter
**
**     Copyright Fraunhofer FOKUS
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <ipfix.h>
#include <mlog.h>
#define PROCERA_PEN    15397 
 
//Common Procera IPFIX fields 
#define IPFIX_IE_SUBSCRIBER_ID          28 
#define IPFIX_IE_USER_LOCATION_INFO     36 

ipfix_field_type_t ipfix_ft_procera[] =
{
    {
        PROCERA_PEN,  IPFIX_IE_SUBSCRIBER_ID,    IPFIX_FT_VARLEN,  IPFIX_CODING_STRING,
        "subscriberId",  "Procera reserved subscriber identification"
    },
    {
        PROCERA_PEN,  IPFIX_IE_USER_LOCATION_INFO,   IPFIX_FT_VARLEN,  IPFIX_CODING_STRING,
        "locationInfo",  "Procera location information"
    }
};



int main(int argc, char **argv)
{
    char      *optstr = "hc:p:vstu";
    int       opt;
    char      chost[256];
    int       protocol = IPFIX_PROTO_TCP;
    int       j;
    uint32_t  bytes    = 1234;
    char ipv6src[]     = { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                           21, 22, 23, 24, 25, 26
                         };
    char ipv6dst[]     = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                           11, 12, 13, 14, 15, 16
                         };
    char ipv4src[]     = {172, 27, 1, 27};
    char ipv4dst[]     = {172, 28, 1, 28};
    char srcport[]     = {11, 11};
    char dstport[]      = {99, 99};
    char *subscriberid = "Daniel1972";
    char *location     = "1276 rue Verdun, Montreal, QC, H9K2J1, Canada";
    uint32_t starttime  = 4000;
    uint32_t stoptime = 6000;
    uint64_t observationpoint = 9999;
    unsigned char tcpproto      = 0x06;
    unsigned char udpproto      = 0x11;
    ipfix_t           *ipfixh  = NULL;
    ipfix_template_t  *ipfixt1  = NULL;
    ipfix_template_t  *ipfixt2  = NULL;
    int               sourceid = 12345;
    int               port     = IPFIX_PORTNO;
    int               verbose_level = 0;
    /* set default host */
    strcpy(chost, "localhost");

    /** process command line args
     */
    while ((opt = getopt(argc, argv, optstr)) != EOF)
    {
        switch (opt)
        {
            case 'p':
                if ((port = atoi(optarg)) < 0)
                {
                    fprintf(stderr, "Invalid -p argument!\n");
                    exit(1);
                }

                break;

            case 'c':
                strcpy(chost, optarg);
                break;

            case 's':
                protocol = IPFIX_PROTO_SCTP;
                break;

            case 't':
                protocol = IPFIX_PROTO_TCP;
                break;

            case 'u':
                protocol = IPFIX_PROTO_UDP;
                break;

            case 'v':
                verbose_level ++;
                break;

            case 'h':
            default:
                fprintf(stderr, "usage: %s [-hstuv] [-c collector] [-p portno]\n"
                        "  -h               this help\n"
                        "  -c <collector>   collector address\n"
                        "  -p <portno>      collector port number (default=%d)\n"
                        "  -s               send data via SCTP\n"
                        "  -t               send data via TCP (default)\n"
                        "  -u               send data via UDP\n"
                        "  -v               increase verbose level\n\n",
                        argv[0], IPFIX_PORTNO);
                exit(1);
        }
    }

    /** init loggin
     */
    mlog_set_vlevel(verbose_level);

    /** init lib
     */
    if (ipfix_init() < 0)
    {
        fprintf(stderr, "cannot init ipfix module: %s\n", strerror(errno));
        exit(1);
    }

    ipfix_add_vendor_information_elements(ipfix_ft_procera);

    /** open ipfix exporter
     */
    if (ipfix_open(&ipfixh, sourceid, IPFIX_VERSION) < 0)
    {
        fprintf(stderr, "ipfix_open() failed: %s\n", strerror(errno));
        exit(1);
    }

    /** set collector to use
     */
    if (ipfix_add_collector(ipfixh, chost, port, protocol) < 0)
    {
        fprintf(stderr, "ipfix_add_collector(%s,%d) failed: %s\n",
                chost, port, strerror(errno));
        exit(1);
    }

    /** get template
     */
    if (ipfix_new_data_template(ipfixh, &ipfixt1, 11) < 0)
    {
        fprintf(stderr, "ipfix_new_template() failed: %s\n",
                strerror(errno));
        exit(1);
    }

    if (ipfix_new_data_template(ipfixh, &ipfixt2, 11) < 0)
    {
        fprintf(stderr, "ipfix_new_template() failed: %s\n",
                strerror(errno));
        exit(1);
    }

    if ((ipfix_add_field(ipfixh, ipfixt1,
                         0, IPFIX_FT_OBSERVATIONPOINTID, 8) < 0)
    	|| (ipfix_add_field(ipfixh, ipfixt1, 0, IPFIX_FT_SOURCEIPV4ADDRESS, 4) < 0)
        || (ipfix_add_field(ipfixh, ipfixt1, 0, IPFIX_FT_SOURCETRANSPORTPORT, 2) < 0)
        || (ipfix_add_field(ipfixh, ipfixt1, 0, IPFIX_FT_DESTINATIONIPV4ADDRESS, 4) < 0)
        || (ipfix_add_field(ipfixh, ipfixt1, 0, IPFIX_FT_DESTINATIONTRANSPORTPORT, 2) < 0)
        || (ipfix_add_field(ipfixh, ipfixt1, 0, IPFIX_FT_PROTOCOLIDENTIFIER, 1) < 0)
        || (ipfix_add_field(ipfixh, ipfixt1, 0, IPFIX_FT_FLOWSTARTSECONDS, 4) < 0)
        || (ipfix_add_field(ipfixh, ipfixt1, 0, IPFIX_FT_FLOWENDSECONDS, 4) < 0)
        || (ipfix_add_field(ipfixh, ipfixt1, PROCERA_PEN, IPFIX_IE_SUBSCRIBER_ID, IPFIX_FT_VARLEN) < 0)
        || (ipfix_add_field(ipfixh, ipfixt1, PROCERA_PEN, IPFIX_IE_USER_LOCATION_INFO, IPFIX_FT_VARLEN) < 0)
        || (ipfix_add_field(ipfixh, ipfixt1,
                            0, IPFIX_FT_PACKETDELTACOUNT, 4) < 0))
    {
        fprintf(stderr, "ipfix_new_template() failed: %s\n",
                strerror(errno));
        exit(1);
    }

    if ((ipfix_add_field(ipfixh, ipfixt2,
                         0, IPFIX_FT_OBSERVATIONPOINTID, 8) < 0)
        || (ipfix_add_field(ipfixh, ipfixt2, 0, IPFIX_FT_SOURCEIPV6ADDRESS, 16) < 0)
        || (ipfix_add_field(ipfixh, ipfixt2, 0, IPFIX_FT_SOURCETRANSPORTPORT, 2) < 0)
        || (ipfix_add_field(ipfixh, ipfixt2, 0, IPFIX_FT_DESTINATIONIPV6ADDRESS, 16) < 0)
        || (ipfix_add_field(ipfixh, ipfixt2, 0, IPFIX_FT_DESTINATIONTRANSPORTPORT, 2) < 0)
        || (ipfix_add_field(ipfixh, ipfixt2, 0, IPFIX_FT_PROTOCOLIDENTIFIER, 1) < 0)
        || (ipfix_add_field(ipfixh, ipfixt2, 0, IPFIX_FT_FLOWSTARTSECONDS, 4) < 0)
        || (ipfix_add_field(ipfixh, ipfixt2, 0, IPFIX_FT_FLOWENDSECONDS, 4) < 0)
        || (ipfix_add_field(ipfixh, ipfixt2, PROCERA_PEN, IPFIX_IE_SUBSCRIBER_ID, IPFIX_FT_VARLEN) < 0)
        || (ipfix_add_field(ipfixh, ipfixt2, PROCERA_PEN, IPFIX_IE_USER_LOCATION_INFO, IPFIX_FT_VARLEN) < 0)

        || (ipfix_add_field(ipfixh, ipfixt2,
                            0, IPFIX_FT_PACKETDELTACOUNT, 4) < 0))
    {
        fprintf(stderr, "ipfix_new_template() failed: %s\n",
                strerror(errno));
        exit(1);
    }

    /** export some data
     */
    for (j = 0; j < 1; j++)
    {
        //printf( "[%d] export some data ... ", j );
        //fflush( stdout) ;
        if (ipfix_export(ipfixh, ipfixt1, &observationpoint, ipv4src, srcport, ipv4dst, dstport, &tcpproto, &starttime, &stoptime, subscriberid, strlen(subscriberid), location, strlen(location), &bytes) < 0)
        {
            fprintf(stderr, "ipfix_export() failed: %s\n",
                    strerror(errno));
            exit(1);
        }


        if (ipfix_export(ipfixh, ipfixt2, &observationpoint, ipv6src, srcport, ipv6dst, dstport, &udpproto, &starttime, &stoptime,  subscriberid, strlen(subscriberid), location, strlen(location), &bytes) < 0)
        {
            fprintf(stderr, "ipfix_export() failed: %s\n",
                    strerror(errno));
            exit(1);
        }

        if (ipfix_export_flush(ipfixh) < 0)
        {
            fprintf(stderr, "ipfix_export_flush() failed: %s\n",
                    strerror(errno));
            exit(1);
        }

        //printf( "done.\n" );
        bytes++;
        //sleep(1);
    }

    printf("data exported.\n");
    /** clean up
     */
    ipfix_delete_template(ipfixh, ipfixt1);
    ipfix_delete_template(ipfixh, ipfixt2);
    ipfix_close(ipfixh);
    ipfix_cleanup();
    exit(0);
}
